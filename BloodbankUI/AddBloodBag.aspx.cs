﻿/*
 *@author: Swarna Shenoy (1870110)
 * @date: 11/28/2019
 * @description: Adds new bag details to DB and updates any bag details with temperature or with reason for its unavailability using REST APIs.
 */
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BloodbankUI
{
    public partial class AddBloodBag : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                //Prepare the blood bag object to be posted
                BloodBag bloodBag = new BloodBag();
                bloodBag.RFIDNo = Convert.ToInt32(txtRFID.Text);
                bloodBag.donorId = Convert.ToInt32(txtDonorID.Text);
                bloodBag.bloodGroup = ddlBloodGroup.SelectedValue;
                bloodBag.entryDate = Convert.ToDateTime(txtEntry.Text);
                bloodBag.expirationDate = Convert.ToDateTime(txtExp.Text);
                bloodBag.bloodComponent = ddlBloodComp.SelectedValue;
                bloodBag.temperature = float.Parse(txtTemp.Text);
                bloodBag.availability = true;

                //URI
                var serviceURI = "https://localhost:44306/bloodbag";

                //HTTP POST Request
                HttpWebRequest serviceRequest = (HttpWebRequest)WebRequest.Create(serviceURI);
                serviceRequest.Method = "POST";
                serviceRequest.ContentType = "application/json";
                serviceRequest.Accept = "application/json";

                string json = JsonConvert.SerializeObject(bloodBag);

                using (var stream = new StreamWriter(serviceRequest.GetRequestStream()))
                {
                    stream.Write(json);
                }

                //HTTP POST Response
                HttpWebResponse serviceResponse = (HttpWebResponse)serviceRequest.GetResponse();             
                if (serviceResponse.StatusCode == HttpStatusCode.OK)
                {
                    txtTemp.Text = "";
                    txtRFID.Text = "";
                    txtExp.Text = "";
                    txtEntry.Text = "";
                    txtDonorID.Text = "";
                    ddlBloodGroup.SelectedValue = "Select";
                    ddlBloodComp.SelectedValue = "Select";
                    lblResult.Text = "Form successfully submitted!!!";
                    lblResult.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lblResult.Text = "OOPS!!! Form submission unsuccessful. Please try again later.";
                lblResult.Visible = true;

                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            tblBloodBag.Visible = true;

            lblRFIDUpdate.Visible = false;
            txtRFIDUpdate.Visible = false;
            lblUpdate.Visible = false;
            txtUpdateTemp.Visible = false;
            btnSubmitTemp.Visible = false;
            lblUpdateResult.Visible = false;
            lblReason.Visible = false;
            txtReason.Visible = false;
            btnSubmitReason.Visible = false;
        }

        protected void btnUpdateTemp_Click(object sender, EventArgs e)
        {
            tblBloodBag.Visible = false;
            lblReason.Visible = false;
            txtReason.Visible = false;
            btnSubmitReason.Visible = false;
            lblUpdateResult.Visible = false;

            lblUpdate.Visible = true;
            txtUpdateTemp.Visible = true;
            lblRFIDUpdate.Visible = true;
            txtRFIDUpdate.Visible = true;
            btnSubmitTemp.Visible = true;
        }

        protected void btnUpdateReason_Click(object sender, EventArgs e)
        {
            tblBloodBag.Visible = false;
            lblUpdate.Visible = false;
            txtUpdateTemp.Visible = false;
            btnSubmitTemp.Visible = false;
            lblUpdateResult.Visible = false;

            lblReason.Visible = true;
            txtReason.Visible = true;
            lblRFIDUpdate.Visible = true;
            txtRFIDUpdate.Visible = true;
            btnSubmitReason.Visible = true;
        }

        protected void btnSubmitTemp_Click(object sender, EventArgs e)
        {
            try
            {
                //URI
                var serviceURI = "https://localhost:44306/bloodBag/temp/" + Convert.ToInt32(txtRFIDUpdate.Text) + "/" + txtUpdateTemp.Text;

                //HTTP PUT Request
                HttpWebRequest serviceRequest = (HttpWebRequest)WebRequest.Create(serviceURI);
                serviceRequest.Method = "PUT";
                serviceRequest.ContentType = "application/json";
                serviceRequest.Accept = "application/json";
                serviceRequest.ContentLength = 0;

                //HTTP PUT Response
                HttpWebResponse serviceResponse = (HttpWebResponse)serviceRequest.GetResponse();          
                if (serviceResponse.StatusCode == HttpStatusCode.OK)
                {
                    txtUpdateTemp.Text = "";
                    txtRFIDUpdate.Text = "";
                    lblUpdateResult.Text = "Updated successfully!!!";
                    lblUpdateResult.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lblUpdateResult.Text = "OOPS!!! Form submission unsuccessful. Please try again later.";
                lblUpdateResult.Visible = true;

                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

        }

        protected void btnSubmitReason_Click(object sender, EventArgs e)
        {
            try
            {
                //URI
                var serviceURI = "https://localhost:44306/bloodBag/" + Convert.ToInt32(txtRFIDUpdate.Text) + "/" + txtReason.Text;

                //HTTP PUT Request
                HttpWebRequest serviceRequest = (HttpWebRequest)WebRequest.Create(serviceURI);
                serviceRequest.Method = "PUT";
                serviceRequest.ContentType = "application/json";
                serviceRequest.Accept = "application/json";
                serviceRequest.ContentLength = 0;

                HttpWebResponse serviceResponse = (HttpWebResponse)serviceRequest.GetResponse();

                //HTTP PUT Response
                if (serviceResponse.StatusCode == HttpStatusCode.OK)
                {
                    txtReason.Text = "";
                    txtRFIDUpdate.Text = "";
                    lblUpdateResult.Text = "Updated successfully!!!";
                    lblUpdateResult.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lblUpdateResult.Text = "OOPS!!! Form submission unsuccessful. Please try again later.";
                lblUpdateResult.Visible = true;

                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

        }

        protected void viewDetails_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ViewBloodBankDetails.aspx");
        }

        protected void donorDetails_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/DonorInfo.aspx");
        }

        protected void regDonor_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Register.aspx");
        }

        protected void weather_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Location.aspx");
        }

        protected void getStock_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/StockExchange.aspx");
        }

        protected void logout_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Login.aspx");
        }
    }
}