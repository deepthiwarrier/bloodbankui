﻿/*
 * @author: Swarna Shneoy (1870110)
 * @date: 11/28/2019
 * @desc: To pass Donor object to the REST API during POST.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BloodbankUI
{
    public class Donor
    {
        public string name
        {
            get; set;
        }

        public int age
        {
            get; set;
        }

        public string gender
        {
            get; set;
        }

        public DateTime dateOfBirth
        {
            get; set;
        }

        public string phone
        {
            get; set;
        }

        public string email
        {
            get; set;
        }

        public string bloodGroup
        {
            get; set;
        }

        public string address
        {
            get; set;
        }
    }
}