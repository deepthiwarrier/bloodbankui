﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BloodBank.Master" AutoEventWireup="true" CodeBehind="Location.aspx.cs" Inherits="BloodbankUI.Location" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p class="auto-style2">
                <asp:Button ID="viewDetails" runat="server" Text="View Details" BorderStyle="Solid" OnClick="viewDetails_Click"/>
                <asp:Button ID="donorDetails" runat="server" Text="Donor Details" BorderStyle="Solid" OnClick="donorDetails_Click"/>
                <asp:Button ID="bloodBag" runat="server" Text="Add/Update Blood Bag" BorderStyle="Solid" OnClick="bloodBag_Click"/>
                <asp:Button ID="regDonor" runat="server" Text="Register Donor" BorderStyle="Solid" OnClick="regDonor_Click"/>
                <asp:Button ID="getStock" runat="server" Text="Get Stock" BorderStyle="Solid" OnClick="getStock_Click"/>
                <asp:Button ID="logout" runat="server" Text="Logout" BorderStyle="Solid" OnClick="logout_Click"/>
            </p>
    <asp:Label ID="Label1" runat="server" Text="Near By Blood Banks:" style="text-align: left; font-weight: 700; color: #000099"></asp:Label>
    <br />
    <asp:Label ID="result" runat="server" Text=""></asp:Label>
    <br />
    <asp:GridView ID="dataResult" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White" />
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <SortedAscendingCellStyle BackColor="#FDF5AC" />
        <SortedAscendingHeaderStyle BackColor="#4D0000" />
        <SortedDescendingCellStyle BackColor="#FCF6C0" />
        <SortedDescendingHeaderStyle BackColor="#820000" />
    </asp:GridView>
    <br />
    <asp:Label ID="Label2" runat="server" Text="Show Hospitals on Map:" style="color: #000099; font-weight: 700"></asp:Label>
    &nbsp;
    <asp:Button ID="Button7" runat="server" Text="Display Map" OnClick="Button7_Click" />    
    <br />
    <asp:Label ID="mapResult" runat="server" Text=""></asp:Label>
    <br />    
    <asp:Image ID="displayImage" runat="server" />   
    </asp:Content>
