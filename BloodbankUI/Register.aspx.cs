﻿/*
 *@author: Swarna Shenoy(1870110)
 * @date: 11/28/2019
 * @description: Adds new donor in DonorDetails Table.
 */
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace BloodbankUI
{
    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }


        protected void btnSubmit_Click1(object sender, EventArgs e)
        { 

            try
            {
                //Prepare the Donor object to be submitted.
                Donor donor = new Donor();
                donor.name = txtName.Text;
                donor.gender = ddlGender.SelectedValue;
                donor.dateOfBirth = Convert.ToDateTime(txtDOB.Text);

                //Using DOB, calculate current age of the donor. 
                int age = 0;
                age = DateTime.Now.Year - donor.dateOfBirth.Year;
                if (DateTime.Now.DayOfYear < donor.dateOfBirth.DayOfYear)
                    age = age - 1;
                donor.age = age;

                donor.phone = txtPhone.Text;
                donor.email = txtEmail.Text;
                donor.bloodGroup = ddlBloodGroup.SelectedValue;
                donor.address = txtAddress.Text;

                //URI
                var serviceURI = "https://localhost:44306/donor";

                //HTTP POST Request
                HttpWebRequest serviceRequest = (HttpWebRequest)WebRequest.Create(serviceURI);
                serviceRequest.Method = "POST";
                serviceRequest.ContentType = "application/json";
                serviceRequest.Accept = "application/json";

                string json = JsonConvert.SerializeObject(donor);

                using (var stream = new StreamWriter(serviceRequest.GetRequestStream()))
                {
                    stream.Write(json);
                }

                //HTTP POST Response
                HttpWebResponse serviceResponse = (HttpWebResponse)serviceRequest.GetResponse();
                if (serviceResponse.StatusCode == HttpStatusCode.OK)
                {
                    txtAddress.Text = "";
                    txtDOB.Text = "";
                    txtEmail.Text = "";
                    txtName.Text = "";
                    txtPhone.Text = "";
                    ddlBloodGroup.SelectedValue = "Select";
                    ddlGender.SelectedValue = "Select";
                    lblResult.Text = "Form successfully submitted!!!";
                    lblResult.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lblResult.Text = "OOPS!!! Form submission unsuccessful. Please try again later.";
                lblResult.Visible = true;

                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

        }

        protected void viewDetails_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ViewBloodBankDetails.aspx");
        }

        protected void donorDetails_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/DonorInfo.aspx");
        }

        protected void bloodBag_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/AddBloodBag.aspx");
        }

        protected void weather_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Location.aspx");
        }

        protected void getStock_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/StockExchange.aspx");
        }

        protected void logout_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Login.aspx");
        }
    }
}