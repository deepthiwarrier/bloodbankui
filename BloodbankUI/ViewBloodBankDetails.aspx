﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BloodBank.Master" AutoEventWireup="true" CodeBehind="ViewBloodBankDetails.aspx.cs" Inherits="BloodbankUI.ViewBloodBankDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style3 {
            color: #003399;
        }
        .auto-style4 {
            text-align: center;
            color: #003399;
            text-decoration: underline;
        }
        .auto-style2 {
            text-align: center;
            color: #000099;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p class="auto-style2">
                <asp:Button ID="donorDetails" runat="server" Text="Donor Details" BorderStyle="Solid" OnClick="donorDetails_Click"/>
                <asp:Button ID="bloodBag" runat="server" Text="Add/Update Blood Bag" BorderStyle="Solid" OnClick="bloodBag_Click"/>
                <asp:Button ID="regDonor" runat="server" Text="Register Donor" BorderStyle="Solid" OnClick="regDonor_Click"/>
                <asp:Button ID="weather" runat="server" Text="Nearby Blood Banks" BorderStyle="Solid" OnClick="weather_Click"/>
                <asp:Button ID="getStock" runat="server" Text="Get Stock" BorderStyle="Solid" OnClick="getStock_Click"/>
                <asp:Button ID="logout" runat="server" Text="Logout" BorderStyle="Solid" OnClick="logout_Click"/>
            </p>
    <p class="auto-style4">
        <strong>View Blood Bank Details</strong></p>
    <p>
        <span class="auto-style3"><strong>Blood Bags Available At the Center:</strong></span>
        <asp:Button ID="Button7" runat="server" Text="Submit" OnClick="Button7_Click" />
        &nbsp;</p>
    <p class="auto-style3">
        <strong>Blood Bags Available for the Blood Group: </strong>
        <asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AutoPostBack="True">
            <asp:ListItem Value="-1">Choose A Blood Group</asp:ListItem>
            <asp:ListItem Value="A+">A+</asp:ListItem>
            <asp:ListItem Value="A-">A-</asp:ListItem>
            <asp:ListItem Value="B+">B+</asp:ListItem>
            <asp:ListItem Value="B-">B-</asp:ListItem>
            <asp:ListItem Value="AB+">AB+</asp:ListItem>
            <asp:ListItem Value="AB-">AB-</asp:ListItem>
            <asp:ListItem Value="O+">O+</asp:ListItem>
            <asp:ListItem Value="O-">O-</asp:ListItem>
        </asp:DropDownList>
    </p>
    <p class="auto-style3">
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/ViewBloodBankDetails.aspx">Show All Blood Bags Details</asp:HyperLink>
    </p>

    <div>
        <asp:Button ID="btnSubmitCHart" runat="server" Text="View History" OnClick="btnSubmitCHart_Click"/>
    </div>
    <br />
    <asp:Label ID="result" runat="server"></asp:Label>
    <asp:GridView ID="dataResult" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White" />
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <SortedAscendingCellStyle BackColor="#FDF5AC" />
        <SortedAscendingHeaderStyle BackColor="#4D0000" />
        <SortedDescendingCellStyle BackColor="#FCF6C0" />
        <SortedDescendingHeaderStyle BackColor="#820000" />
    </asp:GridView>

    <asp:Chart ID="chartBloodInfo" runat="server" Height="200px" Width="600px" Visible="false">
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1">
                <AxisY>
                    <MajorGrid LineColor="DarkGray" LineDashStyle="Dot" />
                </AxisY>
                <AxisX>
                    <MajorGrid LineColor="DarkGray" LineDashStyle="Dot" />
                </AxisX>
            </asp:ChartArea>
        </ChartAreas>
         <Legends>
            <asp:Legend Name="Legend1">
            </asp:Legend>
        </Legends>
    </asp:Chart>
    
    <asp:Label ID="lblResult" runat="server" Text=""></asp:Label>
</asp:Content>
