﻿/*
 *@author: Swarna Shenoy
 * @date: 11/30/2019
 * @description: Retrieves donor information based on RFID and Donor ID.
 */
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BloodbankUI
{
    public partial class DonorInfo : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Visible = false;
            
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                //HTTP GET Request
                HttpWebRequest serviceRequest = (HttpWebRequest)WebRequest.Create("https://localhost:44306/donorByRFID/" + txtID.Text);
                serviceRequest.Method = "GET";
                serviceRequest.ContentLength = 0;
                serviceRequest.ContentType = "text/plain";
                serviceRequest.Accept = "text/plain";

                //HTTP GET Response
                HttpWebResponse serviceResponse = (HttpWebResponse)serviceRequest.GetResponse();
                
                Stream recieveStream = serviceResponse.GetResponseStream();
                Encoding encode = Encoding.GetEncoding("utf-8");
                StreamReader readStream = new StreamReader(recieveStream, encode, true);
                String responseFromServer = readStream.ReadToEnd();

                JavaScriptSerializer js = new JavaScriptSerializer();

                //Deserializing the data inorder to populate onto grid view.
                Dictionary<String, Object> dict = js.Deserialize<Dictionary<String, Object>>(responseFromServer);
                Dictionary<String, Object> tbl = ((Dictionary<String, Object>)((ArrayList)dict["BloodDetails"])[0]);

                DataTable data = new DataTable();
                foreach (string str in tbl.Keys)
                {
                    data.Columns.Add(str);    
                }
                data.Rows.Add(tbl.Values.ToArray<Object>());

                grvDonor.DataSource = data;
                grvDonor.DataBind();
                grvDonor.Visible = true;
                lblDonorID.Visible = false;;
                btnSubmitDonor.Visible = false;
                txtDonorId.Visible = false;
            }
            catch(Exception ex)
            {
                if (ex.Message.Contains("400"))
                {
                    lblMessage.Text = "Invalid RFID";
                    lblMessage.Visible = true;
                }
                else
                {
                    lblMessage.Text = "OOPS!!!! Something went wrong, please try again later.";
                    lblMessage.Visible = true;
                }
                grvDonor.Visible = false;               
                lblDonorID.Visible = false;
                btnSubmitDonor.Visible = false;
                txtDonorId.Visible = false;
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            
        }

        protected void ddlID_SelectedIndexChanged1(object sender, EventArgs e)
        {
            if (ddlID.SelectedValue.Equals("Select"))
            {
                lblSelect.Visible = true;
                lblRFID.Visible = false;
                txtID.Visible = false;
                btnSubmit.Visible = false;
                lblDonorID.Visible = false;
                txtDonorId.Visible = false;
                btnSubmitDonor.Visible = false;
                grvDonor.Visible = false;
            }
            else if (ddlID.SelectedValue.Equals("By RFID"))
            {
                lblRFID.Visible = true;
                txtID.Visible = true;
                btnSubmit.Visible = true;
                lblDonorID.Visible = false;
                txtDonorId.Visible = false;
                btnSubmitDonor.Visible = false;
                grvDonor.Visible = false;
                lblSelect.Visible = false;
                txtID.Text = "";
                txtDonorId.Text = "";
            }
            else if (ddlID.SelectedValue.Equals("By DonorID"))
            {
                lblRFID.Visible = false;
                txtID.Visible = false;
                lblDonorID.Visible = true;
                txtDonorId.Visible = true;
                txtID.Text = "";
                txtDonorId.Text = "";
                btnSubmit.Visible = false;
                btnSubmitDonor.Visible = true;
                grvDonor.Visible = false;
                lblSelect.Visible = false;
            }

        }

        protected void viewDetails_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ViewBloodBankDetails.aspx");
        }

        protected void regDonor_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Register.aspx");
        }

        protected void bloodBag_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/AddBloodBag.aspx");
        }

        protected void weather_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Location.aspx");
        }

        protected void getStock_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/StockExchange.aspx");
        }

        protected void logout_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Login.aspx");
        }

        protected void btnSubmitDonor_Click(object sender, EventArgs e)
        {
            try
            {
                //HTTP GET request
                HttpWebRequest serviceRequest = (HttpWebRequest)WebRequest.Create("https://localhost:44306/donor/" + txtDonorId.Text);
                serviceRequest.Method = "GET";
                serviceRequest.ContentLength = 0;
                serviceRequest.ContentType = "text/plain";
                serviceRequest.Accept = "text/plain";

                //HTTP GET response
                HttpWebResponse serviceResponse = (HttpWebResponse)serviceRequest.GetResponse();
               
                Stream recieveStream = serviceResponse.GetResponseStream();
                Encoding encode = Encoding.GetEncoding("utf-8");
                StreamReader readStream = new StreamReader(recieveStream, encode, true);
                String responseFromServer = readStream.ReadToEnd();

                JavaScriptSerializer js = new JavaScriptSerializer();
                
                //Deserializing the data inorder to populate onto grid view.

                Dictionary<String, Object> dict = js.Deserialize<Dictionary<String, Object>>(responseFromServer);
                Dictionary<String, Object> tbl = ((Dictionary<String, Object>)((ArrayList)dict["BloodDetails"])[0]);

                DataTable data = new DataTable();
                foreach (string str in tbl.Keys)
                {
                    data.Columns.Add(str);
                }
                data.Rows.Add(tbl.Values.ToArray<Object>());

                grvDonor.DataSource = data;
                grvDonor.DataBind();
                grvDonor.Visible = true;
                lblRFID.Visible = false;
                btnSubmit.Visible = false;
                txtID.Visible = false;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("400"))
                {
                    lblMessage.Text = "Invalid DonorId";
                    lblMessage.Visible = true;

                }
                else
                {
                    lblMessage.Text = "OOPS!!!! Something went wrong, please try again later.";
                    lblMessage.Visible = true;
                }
                grvDonor.Visible = false;                
                lblRFID.Visible = false;
                btnSubmit.Visible = false;
                txtID.Visible = false;
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

        }


    }
}