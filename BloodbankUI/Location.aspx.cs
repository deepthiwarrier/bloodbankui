﻿/**
 * Location.aspx.cs
 * @Author : Deepthi Warrier
 * @Student ID: 1970749
 * @Date: 06/Dec/2019
 * Lists all the bloodbanks available near UWTacoma. Also shows a map.
 */

using Newtonsoft.Json;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.UI.WebControls;


namespace BloodbankUI
{
    public partial class Location : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mapResult.Visible = false;
            result.Visible = false;
            //On page load, displays a list of near by hospitals/bloodbanks
            DataSet bloodBanks = getBloodBanks();
            displayGrid(bloodBanks);
        }

        // Method that calls the bloodbanks service to retrieve the
        //list of near by bloodbanks/Hospitals
        public DataSet getBloodBanks()
        {
            var serviceURL = "https://localhost:44306/bloodbanks";
            DataSet dsBloodBanks = new DataSet();
            try
            {
                // Request the REST service method
                HttpWebRequest serviceRequest = (HttpWebRequest)WebRequest.Create(serviceURL);
                serviceRequest.Method = "GET";
                serviceRequest.ContentLength = 0;
                serviceRequest.ContentType = "text/plain; charset=utf-8";

                //Get the HttpWebresponse
                HttpWebResponse serviceResponse = (HttpWebResponse)serviceRequest.GetResponse();
                Stream receiveStream = serviceResponse.GetResponseStream();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                StreamReader readStream = new StreamReader(receiveStream, encode, true);
                string responseFromServer = readStream.ReadToEnd();

                dsBloodBanks = JsonConvert.DeserializeObject<DataSet>(responseFromServer);              
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);                
            }
            return dsBloodBanks;
        }
        
        // Display the list of hospitals in a Table format
        public void displayGrid(DataSet dsBloodbanks)
        {
            DataSet dsBloodBankDetails = new DataSet();            
            try
            {
                var rowCount = dsBloodbanks.Tables[0].Rows.Count;
                if (rowCount > 0)
                {
                    // Get the hospital/bloodbank names from the dataset returned
                    var hospitalNames = dsBloodbanks.Tables[0].AsEnumerable().Select(r => r.Field<string>("hospitalName")).ToList();
                    
                    DataTable dt = new DataTable("HospitalNames");
                    dt.Columns.Add(new DataColumn("S.No", typeof(int)));
                    dt.Columns.Add(new DataColumn("Hospital Name", typeof(string)));
                   
                    for (var i=0; i<rowCount; i++)
                    {
                        DataRow dr = dt.NewRow();
                        dr["S.No"] = i+1;
                        dr["Hospital Name"] = hospitalNames[i];
                        dt.Rows.Add(dr);
                    }
                    dsBloodBankDetails.Tables.Add(dt);

                    dataResult.DataSource = dsBloodBankDetails;
                    dataResult.DataBind();
                    dataResult.Visible = true;
                }           
            }
            catch (Exception ex)
            {
                //handle any exceptions - Display a user friendly message.
                System.Diagnostics.Debug.WriteLine(ex.Message);
                dataResult.Visible = false;
                result.Visible = true;
                result.ForeColor = System.Drawing.Color.Red;
                result.Text = "Data currently not available.";
            }
        }

        // Display a Map of the near by Bloodbanks.
        protected void Button7_Click(object sender, EventArgs e)
        {
            var serviceURL = "http://localhost:8080/LocationMap/map";            
            try
            {                
                HttpWebRequest serviceRequest = (HttpWebRequest)WebRequest.Create(serviceURL);
                serviceRequest.Method = "GET";
                serviceRequest.ContentLength = 0;
                serviceRequest.ContentType = "image/jpeg; encoding='utf-8";
                
                using (HttpWebResponse serviceResponse = (HttpWebResponse)serviceRequest.GetResponse())
                {
                    using (BinaryReader reader = new BinaryReader(serviceResponse.GetResponseStream()))
                    {
                        Byte[] lnByte = reader.ReadBytes(1 * 1024 * 1024 * 10);
                        using (FileStream fileStream = new FileStream(Server.MapPath("~\\Myimage.jpg"), FileMode.Create))
                        {
                            fileStream.Write(lnByte, 0, lnByte.Length);
                        }
                    }
                }
                string imgPath = "../Myimage.jpg";
                displayImage.ImageUrl = imgPath;         
                displayImage.Visible = true;         
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                mapResult.Visible = true;
                mapResult.ForeColor = System.Drawing.Color.Red;
                mapResult.Text = "Data currently not available.";
            }
        }

        // Navigation to the Other pages on the site.
        // View Blood Bank Details, Donor Info Page, Register Donor Page etc.
        protected void viewDetails_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ViewBloodBankDetails.aspx");
        }

        protected void donorDetails_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/DonorInfo.aspx");
        }

        protected void regDonor_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Register.aspx");
        }
        protected void bloodBag_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/AddBloodBag.aspx");
        }

        protected void getStock_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/StockExchange.aspx");
        }

        protected void logout_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Login.aspx");
        }
    }
}