﻿/*
 * @author: Swarna Shneoy (1870110)
 * @date: 11/30/2019
 * @desc: To pass BloodBag object to the REST API during POST.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BloodbankUI
{
    public class BloodBag
    {
        public int RFIDNo
        {
            get; set;
        }

        public int donorId
        {
            get; set;
        }

        public string bloodGroup
        {
            get; set;
        }

        public DateTime entryDate
        {
            get; set;
        }

        public DateTime expirationDate
        {
            get; set;
        }

        public string bloodComponent
        {
            get; set;
        }

        public float temperature
        {
            get; set;
        }

        public Boolean availability
        {
            get; set;
        }
    }
}