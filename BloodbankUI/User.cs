﻿/**
 * User.cs
 * @Author : Deepthi Warrier
 * @Student ID: 1970749
 * @Date: 06/Dec/2019
 * User Model to pass the username and password to the REST service for validation.
 * Used for the Login operation
 */

namespace BloodbankUI
{
    public class User
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}