﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BloodBank.Master" AutoEventWireup="true" CodeBehind="StockExchange.aspx.cs" Inherits="BloodbankUI.StockExchange" %>
<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style3 {
            text-align: center;
            color: #003399;
            text-decoration: underline;
        }
        .auto-style4 {
            text-align: left;
            color: #003399;
        }
        .auto-style5 {
            color: #000099;
        }
        .auto-style2 {
            text-align: center;
            color: #000099;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p class="auto-style2">
                <asp:Button ID="viewDetails" runat="server" Text="View Details" BorderStyle="Solid" OnClick="viewDetails_Click"/>
                <asp:Button ID="donorDetails" runat="server" Text="Donor Details" BorderStyle="Solid" OnClick="donorDetails_Click"/>
                <asp:Button ID="bloodBag" runat="server" Text="Add/Update Blood Bag" BorderStyle="Solid" OnClick="bloodBag_Click"/>
                <asp:Button ID="regDonor" runat="server" Text="Register Donor" BorderStyle="Solid" OnClick="regDonor_Click"/>
                <asp:Button ID="weather" runat="server" Text="Nearby Blood Banks" BorderStyle="Solid" OnClick="weather_Click"/>
                <asp:Button ID="logout" runat="server" Text="Logout" BorderStyle="Solid" OnClick="logout_Click"/>
            </p>
    <p class="auto-style3">
        <strong>Stock Exchange Information</strong></p>
    <p class="auto-style4">
        <strong>Stock Prices For the Top Medical Firms.</strong></p>
    <p class="auto-style4">
        <asp:Label ID="result" runat="server"></asp:Label>
    </p>       
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" OnLoad="UpdatePanel1_Load">
        <ContentTemplate>
            <asp:Timer ID="Timer1" Interval="10000" runat="server" OnTick="Timer1_Tick"></asp:Timer>
            <asp:GridView ID="dataResult" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None">
                <AlternatingRowStyle BackColor="White" />
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <SortedAscendingCellStyle BackColor="#FDF5AC" />
                <SortedAscendingHeaderStyle BackColor="#4D0000" />
                <SortedDescendingCellStyle BackColor="#FCF6C0" />
                <SortedDescendingHeaderStyle BackColor="#820000" />
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <br />
    <span class="auto-style5"><strong>Stock Prices Chart Display: </strong> </span>
    <asp:Button ID="submitBtn" runat="server" Text="Submit" OnClick="submitBtn_Click" />
    <br />
    <asp:Chart ID="chartDisplay" runat="server">
        <Series>
            <asp:Series Name="Series1" ChartType="StackedColumn" YValuesPerPoint="4">
            </asp:Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1">
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
    </asp:Content>
