﻿/**
 * StockExchange.aspx.cs
 * @Author : Deepthi Warrier
 * @Student ID: 1970749
 * @Date: 06/Dec/2019
 * Diplays the current Stock prices for the top 5 medical firms.
 * This is achieved using Ajax.
 * Visualization in the form of a chart is also provided.
 */
using Newtonsoft.Json;
using System;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace BloodbankUI
{
    public partial class StockExchange : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {                        
        }

        // Get the Stock prices for the top five companies.
        public DataSet getStockDetails()
        {
            string companyUrl = "https://localhost:44306/companies";
            string companies = "";
            DataSet dsStockDetails = new DataSet();

            try
            {
                string responseFromServer = callService(companyUrl);
                DataSet dsCompanies = JsonConvert.DeserializeObject<DataSet>(responseFromServer);

                foreach (DataRow row in dsCompanies.Tables[0].Rows)
                {
                    companies += (string)row["companySymbol"] + ",";
                }
                companies = companies.TrimEnd(',');

                // Use the already existing api - world trading data to retrieve the prices.
                string serviceUrl = "https://api.worldtradingdata.com/api/v1/stock?symbol=" + companies + "&api_token=DsPSIXwePruLOB9aeKNL6EJCSqnswGMHDpIbW94VBQOvHqCt4SHVWaspBPpu";

                responseFromServer = callService(serviceUrl);

                JavaScriptSerializer js = new JavaScriptSerializer();
                var obj = js.Deserialize<dynamic>(responseFromServer);

                DataTable table = new DataTable("StockDetails");
                table.Columns.Add("Stock Name", typeof(string));
                table.Columns.Add("Stock Price", typeof(float));

                for (var i = 0; i < 5; i++)
                {
                    table.Rows.Add(obj["data"][i]["name"], obj["data"][i]["price"]);
                }
                dsStockDetails.Tables.Add(table);
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            return dsStockDetails;
        }
        
        // Display the stock prices in a table format
        public void showTable(DataSet dsStockDetails)
        {
            try
            {
                if (dsStockDetails.Tables[0].Rows.Count != 0)
                {
                    dataResult.DataSource = dsStockDetails;
                    dataResult.DataBind();
                    dataResult.Visible = true;
                }
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                dataResult.Visible = false;
                result.ForeColor = System.Drawing.Color.Red;
                result.Text = "Data currently not available.";
            }            
        }

        // Genric method to call services and returned the response.
        public string callService(string serviceurl)
        {
            string responseFromServer = "";
            try
            {
                // Create Request to the service
                HttpWebRequest serviceRequest = (HttpWebRequest)WebRequest.Create(serviceurl);
                serviceRequest.Method = "GET";
                serviceRequest.ContentLength = 0;
                serviceRequest.ContentType = "text/plain";
                serviceRequest.Accept = "text/plain";

                //Get the response
                HttpWebResponse serviceResponse = (HttpWebResponse)serviceRequest.GetResponse();
                Stream receiveStream = serviceResponse.GetResponseStream();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                StreamReader readStream = new StreamReader(receiveStream, encode, true);
                responseFromServer = readStream.ReadToEnd();                
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            return responseFromServer;
        }

        // Display a chart of the stock prices.
        public void displayChart(DataSet stockDetails)
        {
            try
            {
                chartDisplay.DataSource = stockDetails;
                chartDisplay.Series["Series1"].XValueMember = "Stock Name";
                chartDisplay.Series["Series1"].YValueMembers = "Stock Price";                
                chartDisplay.Visible = true;
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        // Display chart on click of submit button.
        protected void submitBtn_Click(object sender, EventArgs e)
        {
            DataSet stockDetails = getStockDetails();
            displayChart(stockDetails);
        }

        //The panel of Stock prices gets updated every 10seconds
        protected void Timer1_Tick(object sender, EventArgs e)
        {
            DataSet stockDetails = getStockDetails();
            showTable(stockDetails);
            chartDisplay.Visible = false;
        }

        //Panel used to display the table of stock prices on load
        protected void UpdatePanel1_Load(object sender, EventArgs e)
        {
            DataSet stockDetails = getStockDetails();
            showTable(stockDetails);
            chartDisplay.Visible = false;
        }

        // Navigation to other pages in the BloodBank website.
        protected void viewDetails_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ViewBloodBankDetails.aspx");
        }

        protected void donorDetails_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/DonorInfo.aspx");
        }

        protected void regDonor_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Register.aspx");
        }

        protected void weather_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Location.aspx");
        }

        protected void bloodBag_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/AddBloodBag.aspx");
        }

        protected void logout_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Login.aspx");
        }
    }
}