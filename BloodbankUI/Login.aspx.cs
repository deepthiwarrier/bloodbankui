﻿/**
 * Login.aspx.cs
 * @Author : Deepthi Warrier
 * @Student ID: 1970749
 * @Date: 06/Dec/2019
 * Authenticates the user and Redirects to ViewDetails Page
 * 1. if user is valid, else
 * 2. shows a message box and redirects to the same login page. * 
 */

using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace BloodbankUI
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        // Validates username and password entered by the user
        protected void Button1_Click(object sender, EventArgs e)
        {
            User user = new User();
            user.username = username.Text;
            user.password = password.Text;
            dynamic obj;
            try
            {
                // Calls the login service to validate the user
                var serviceURL = "https://localhost:44306/login";

                HttpWebRequest serviceRequest = (HttpWebRequest)WebRequest.Create(serviceURL);
                serviceRequest.Method = "POST";
                serviceRequest.ContentType = "application/json";
                serviceRequest.Accept = "application/json";

                // Data passed in as JSON
                string json = JsonConvert.SerializeObject(user);
                
                using (var stream = new StreamWriter(serviceRequest.GetRequestStream()))
                {
                    stream.Write(json);                    
                }

                // Service Response
                HttpWebResponse serviceResponse = (HttpWebResponse)serviceRequest.GetResponse();
                Stream receiveStream = serviceResponse.GetResponseStream();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                StreamReader readStream = new StreamReader(receiveStream, encode, true);
                string serviceResult = readStream.ReadToEnd();

                // Reads the response returned and redirects to the correct page
                JavaScriptSerializer js = new JavaScriptSerializer();
                obj = js.Deserialize<dynamic>(serviceResult);

                // If the user is valid - redirect to the Viewdetails page
                if (obj == true)
                {
                    Response.Redirect("~/ViewBloodBankDetails.aspx");
                }
                else
                {
                    // If the user is invalid, shows a message box- Not a valid user.
                    DialogResult dr = MessageBox.Show("Not a Valid User. Please try again!");
                    if (dr == DialogResult.OK)
                        Response.Redirect("~/Login.aspx");
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }
    }
}