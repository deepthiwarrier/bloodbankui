﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BloodBank.Master" AutoEventWireup="true" CodeBehind="AddBloodBag.aspx.cs" Inherits="BloodbankUI.AddBloodBag" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style3 {
            font-weight: normal;
            text-align: center;
            color: #003399;
            text-decoration: underline;
        }
        .auto-style2 {
            text-align: center;
            color: #000099;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p class="auto-style2">
                <asp:Button ID="viewDetails" runat="server" Text="View Details" BorderStyle="Solid" OnClick="viewDetails_Click"/>
                <asp:Button ID="donorDetails" runat="server" Text="Donor Details" BorderStyle="Solid" OnClick="donorDetails_Click"/>
                <asp:Button ID="regDonor" runat="server" Text="Register Donor" BorderStyle="Solid" OnClick="regDonor_Click"/>
                <asp:Button ID="weather" runat="server" Text="Nearby Blood Banks" BorderStyle="Solid" OnClick="weather_Click"/>
                <asp:Button ID="getStock" runat="server" Text="Get Stock" BorderStyle="Solid" OnClick="getStock_Click"/>
                <asp:Button ID="logout" runat="server" Text="Logout" BorderStyle="Solid" OnClick="logout_Click"/>
            </p>
    <h2 class="auto-style3">
        <strong>Add/Update Blood Bag</strong></h2>

    <div>
        <asp:Button ID="btnAdd" runat="server" Text="Add Details" Font-Bold="true" OnClick ="btnAdd_Click"/>
        <asp:Button ID="btnUpdateTemp" runat="server" Text="Update Temperature" Font-Bold="true" OnClick="btnUpdateTemp_Click"/>
        <asp:Button ID="btnUpdateReason" runat="server" Text="Update Availability" Font-Bold="true" OnClick="btnUpdateReason_Click"/>
    </div>

    <div>
        <asp:Table ID="tblBloodBag" runat="server" HorizontalAlign="Center" Visible ="false">
        <asp:TableRow>
            <asp:TableCell><asp:Label ID="lRFID" runat="server" Text="RFID No. " Font-Bold="true"></asp:Label></asp:TableCell>
            <asp:TableCell><asp:TextBox ID="txtRFID" runat="server"></asp:TextBox></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label ID="lblDonorID" runat="server" Text="Donor ID " Font-Bold="true"></asp:Label></asp:TableCell>
            <asp:TableCell><asp:TextBox ID="txtDonorID" runat="server"></asp:TextBox></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label ID="lBloodGroup" runat="server" Text="Blood Group " Font-Bold="true"></asp:Label></asp:TableCell>
            <asp:TableCell><asp:DropDownList ID="ddlBloodGroup" runat="server">
                <asp:ListItem>Select</asp:ListItem>
                <asp:ListItem>A+</asp:ListItem>
                <asp:ListItem>A-</asp:ListItem>
                <asp:ListItem>B+</asp:ListItem>
                <asp:ListItem>B-</asp:ListItem>
                <asp:ListItem>O+</asp:ListItem>
                <asp:ListItem>O-</asp:ListItem>
                <asp:ListItem>AB+</asp:ListItem>
                <asp:ListItem>AB-</asp:ListItem>
                </asp:DropDownList></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label ID="lblEntry" runat="server" Text="Entry Date(yyyy-mm-dd) " Font-Bold="true"></asp:Label></asp:TableCell>
            <asp:TableCell><asp:TextBox ID="txtEntry" runat="server"></asp:TextBox></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label ID="lblExp" runat="server" Text="Expiration Date(yyyy-mm-dd) " Font-Bold="true"></asp:Label></asp:TableCell>
            <asp:TableCell><asp:TextBox ID="txtExp" runat="server"></asp:TextBox></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label ID="lblBloodComp" runat="server" Text="Blood Component " Font-Bold="true"></asp:Label></asp:TableCell>
            <asp:TableCell><asp:DropDownList ID="ddlBloodComp" runat="server">
                <asp:ListItem>Select</asp:ListItem>                
                <asp:ListItem>Plasma</asp:ListItem>
                <asp:ListItem>Platelets</asp:ListItem>
                <asp:ListItem>RBC</asp:ListItem>
            </asp:DropDownList></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label ID="lTemp" runat="server" Text="Temperature(Fahrenheit) " Font-Bold="true"></asp:Label></asp:TableCell>
            <asp:TableCell><asp:TextBox ID="txtTemp" runat="server"></asp:TextBox></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow HorizontalAlign="Center">
            <asp:TableCell HorizontalAlign ="Center">
                  <asp:Button ID="btnSubmit" runat="server" Text="SUBMIT" Font-Bold="true" OnClick="btnSubmit_Click"/> </asp:TableCell>
            </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="lblResult" runat="server" Text="" Visible="false" ForeColor="Green" Font-Bold="true"></asp:Label></asp:TableCell>
        </asp:TableRow>
        </asp:Table>
    </div>
    <div>
        <asp:Label ID="lblRFIDUpdate" runat="server" Text="RFID No." Visible="false" Font-Bold="true"></asp:Label>
        <asp:TextBox ID="txtRFIDUpdate" runat="server" Visible="false" Font-Bold="true"></asp:TextBox>
        <asp:Label ID="lblUpdate" runat="server" Text="Temperature " Font-Bold="true" Visible="false"></asp:Label>
        <asp:TextBox ID="txtUpdateTemp" runat="server" Visible="false"></asp:TextBox>
        <asp:Label ID="lblReason" runat="server" Text="Reason " Font-Bold="true" Visible="false"></asp:Label>
        <asp:TextBox ID="txtReason" runat="server" Visible="false"></asp:TextBox>
        <asp:Button ID="btnSubmitTemp" runat="server" Text="SUBMIT" Visible="false" Font-Bold="true" OnClick="btnSubmitTemp_Click"/>
        <asp:Button ID="btnSubmitReason" runat="server" Text="SUBMIT" Visible="false" Font-Bold="true" OnClick="btnSubmitReason_Click"/>
        <asp:Label ID="lblUpdateResult" runat="server" Text="" Visible="false" Font-Bold="true" ForeColor="Green"></asp:Label>
        
    </div>
<p>
    &nbsp;</p>
<p>
    &nbsp;</p>
</asp:Content>
