﻿/**
 * ViewBloodBankDetails.aspx.cs
 * @Author : Deepthi Warrier
 * @Student ID: 1970749
 * @Date: 06/Dec/2019
 * Diplays the Blood Bag Related Details.
 * 1. Displays all the avaiable blood bags in the center
 * 2. Displays the total count for each blood bag
 * 3. Displays the count of components for just a blood group
 * 4. Provides Visualization of the previous blood data
 */

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Web.UI.DataVisualization.Charting;

namespace BloodbankUI
{
    public partial class ViewBloodBankDetails : System.Web.UI.Page
    {
        // Displays the data avaialble on page load
        protected void Page_Load(object sender, EventArgs e)
        {
            lblResult.Text = "";
            result.Visible = false;
            var serviceURL = "https://localhost:44306/blooddetails";
            // Display the data in a table format
            try
            {
                displayGridView(serviceURL);
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }            
        }

        // Displays the count for each blood group available at the center
        protected void Button7_Click(object sender, EventArgs e)
        {
            var serviceURL = "https://localhost:44306/count";

            try
            {
                displayGridView(serviceURL);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }                    
        }

        // Code to display the content in a table format
        public void displayGridView(string serviceURL)
        {
            try
            {
                // Creates a request
                HttpWebRequest serviceRequest = (HttpWebRequest)WebRequest.Create(serviceURL);
                serviceRequest.Method = "GET";
                serviceRequest.ContentLength = 0;
                serviceRequest.ContentType = "text/plain; charset=utf-8";

                //Get the reponse
                HttpWebResponse serviceResponse = (HttpWebResponse)serviceRequest.GetResponse();
                Stream receiveStream = serviceResponse.GetResponseStream();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                StreamReader readStream = new StreamReader(receiveStream, encode, true);
                string responseFromServer = readStream.ReadToEnd();

                DataSet dsBloodDetails = JsonConvert.DeserializeObject<DataSet>(responseFromServer);
                dataResult.DataSource = dsBloodDetails;
                dataResult.DataBind();
                dataResult.Visible = true;
            }catch(Exception ex)
            {
                //Handle any Exceptions to dispaly user friendly error message
                System.Diagnostics.Debug.WriteLine(ex.Message);
                dataResult.Visible = false;
                result.Visible = true;
                result.ForeColor = System.Drawing.Color.Red;
                result.Text = "Data currently not available.";
            }
        }

        // Display the count and component details for a specific blood group
        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnSubmitCHart.Visible = true;
            var value = DropDownList1.SelectedItem.Value;
            try
            {
                // If the user selects the right bloodgroup.
                if (value != "-1")
                {
                    var serviceURL = "https://localhost:44306/count/" + value;
                    displayGridView(serviceURL);
                }
                else
                {
                    // If the user didnot select a bloodgroup from the drop down list, Display an Error Message.
                    dataResult.Visible = false;
                    result.Visible = true;
                    result.ForeColor = System.Drawing.Color.Red;
                    result.Text = "Please select a bloodgroup from the list";
                }
            }
            catch (Exception ex)
            {
                dataResult.Visible = false;
                result.ForeColor = System.Drawing.Color.Red;
                result.Text = ex.Message;
            }
        }

        // Navigation to all the other pages available on the website
        protected void bloodBag_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/AddBloodBag.aspx");
        }

        protected void donorDetails_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/DonorInfo.aspx");
        }

        protected void regDonor_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Register.aspx");
        }

        protected void weather_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Location.aspx");
        }

        protected void getStock_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/StockExchange.aspx");
        }

        protected void logout_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Login.aspx");
        }

        //@author: Swarna Shenoy
        //@date: 11/30/2019 
        //@desc: Visualization of blood bank history.
        protected void btnSubmitCHart_Click(object sender, EventArgs e)
        {
            dataResult.Visible = false;
            btnSubmitCHart.Visible = false;
            chartBloodInfo.Visible = true;
            try
            {
                //Invoking a SOAP client
                BloodCountInfo.SoapWSSoapClient openDBWS = new BloodCountInfo.SoapWSSoapClient();
                DataSet ds = openDBWS.getDisplayInfo();
                DataTable dt = ds.Tables["BloodBagInfo"];

                //Code for visualization of data onto the chart.

                //Building a map with <bloodGroup : <Month, Count>> from the DataTable retrieved from SOAP client.
                Dictionary<string, Dictionary<int, int>> bloodCountMap = new Dictionary<string, Dictionary<int, int>>();
                string[] arr = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
                foreach (DataRow dr in dt.Rows)
                {
                    if (!bloodCountMap.ContainsKey((string)dr["bloodGroup"]))
                    {
                        bloodCountMap.Add((string)dr["bloodGroup"], new Dictionary<int, int>());
                    }
                    Dictionary<int, int> countMap;
                    bloodCountMap.TryGetValue((string)dr["bloodGroup"], out countMap);
                    countMap.Add((int)dr["Month"], (int)dr["Count"]);
                }


                //For visualization populating chart with the count (on y-axis) and blood group for each month (x-axis)
                foreach (string group in bloodCountMap.Keys)
                {
                    Series s = new Series(string.Format("{0}", group));
                    s.ChartType = SeriesChartType.Column;
                    Dictionary<int, int> countMap;
                    bloodCountMap.TryGetValue(group, out countMap);
                    int i = 0;
                    foreach (int month in countMap.Keys)
                    {
                        int count;
                        if (!countMap.TryGetValue(month, out count))
                        {
                            count = 0;
                        }
                        s.Points.AddXY(month, new object[] { count });
                        s.Points[i].AxisLabel = arr[month - 1];
                        i += 1;
                    }
                    chartBloodInfo.Series.Add(s);
                }
                chartBloodInfo.Visible = true;

            }
            catch (Exception ex)
            {
                chartBloodInfo.Visible = false;
                lblResult.Text = "Oops, something went wrong!!" + ex.Message;
            }
        }
    }
}