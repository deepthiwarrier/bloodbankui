﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BloodBank.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="BloodbankUI.Register" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style3 {
            text-align: center;
            color: #003399;
            text-decoration: underline;
        }
        .auto-style2 {
            text-align: center;
            color: #000099;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <p class="auto-style2">
                <asp:Button ID="viewDetails" runat="server" Text="View Details" BorderStyle="Solid" OnClick="viewDetails_Click"/>
                <asp:Button ID="donorDetails" runat="server" Text="Donor Details" BorderStyle="Solid" OnClick="donorDetails_Click"/>
                <asp:Button ID="bloodBag" runat="server" Text="Add/Update Blood Bag" BorderStyle="Solid" OnClick="bloodBag_Click"/>
                <asp:Button ID="weather" runat="server" Text="Nearby Blood Banks" BorderStyle="Solid" OnClick="weather_Click"/>
                <asp:Button ID="getStock" runat="server" Text="Get Stock" BorderStyle="Solid" OnClick="getStock_Click"/>
                <asp:Button ID="logout" runat="server" Text="Logout" BorderStyle="Solid" OnClick="logout_Click"/>
            </p>
    <h2 class="auto-style3">
        <strong>Register Donor</strong></h2>
    <div>
        <asp:Table ID="tblRegistration" runat="server" HorizontalAlign="Center">
        <asp:TableRow>
            <asp:TableCell><asp:Label ID="lName" runat="server" Text="Full Name" Font-Bold="true"></asp:Label></asp:TableCell>
            <asp:TableCell><asp:TextBox ID="txtName" runat="server"></asp:TextBox></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label ID="lBloodGroup" runat="server" Text="Blood Group" Font-Bold="true"></asp:Label></asp:TableCell>
            <asp:TableCell><asp:DropDownList ID="ddlBloodGroup" runat="server">
                <asp:ListItem Value="Select">Select</asp:ListItem>
                <asp:ListItem Value="A+">A+</asp:ListItem>
                <asp:ListItem Value="A-">A-</asp:ListItem>
                <asp:ListItem Value="B+">B+</asp:ListItem>
                <asp:ListItem Value="B-">B-</asp:ListItem>
                <asp:ListItem Value="O+">O+</asp:ListItem>
                <asp:ListItem Value="O-">O-</asp:ListItem>
                <asp:ListItem Value="AB+">AB+</asp:ListItem>
                <asp:ListItem Value="AB-">AB-</asp:ListItem>
                </asp:DropDownList></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label ID="lPhone" runat="server" Text="Phone" Font-Bold="true"></asp:Label></asp:TableCell>
            <asp:TableCell><asp:TextBox ID="txtPhone" runat="server"></asp:TextBox></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label ID="lEmail" runat="server" Text="e-mail" Font-Bold="true"></asp:Label></asp:TableCell>
            <asp:TableCell><asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label ID="lGender" runat="server" Text="Gender" Font-Bold="true"></asp:Label></asp:TableCell>
            <asp:TableCell><asp:DropDownList ID="ddlGender" runat="server">
                <asp:ListItem>Select</asp:ListItem>
                <asp:ListItem>Female</asp:ListItem>
                <asp:ListItem>Male</asp:ListItem>
                <asp:ListItem>Other</asp:ListItem>
            </asp:DropDownList></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label ID="lblDOB" runat="server" Text="Date of Birth (yyyy-mm-dd)" Font-Bold="true"></asp:Label></asp:TableCell>
            <asp:TableCell><asp:TextBox ID="txtDOB" runat="server"></asp:TextBox></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label ID="lAddress" runat="server" Text="Address" Font-Bold="true"></asp:Label></asp:TableCell>
            <asp:TableCell><asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine"></asp:TextBox></asp:TableCell>
        </asp:TableRow>
            <asp:TableRow HorizontalAlign="Center">
                <asp:TableCell HorizontalAlign ="Center">
                    <asp:Button ID="btnSubmit" runat="server" Text="SUBMIT"  Font-Bold="true" OnClick="btnSubmit_Click1"/> </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
               <asp:TableCell HorizontalAlign="Center">
                   <asp:Label ID="lblResult" runat="server" Text="" Visible="false" ForeColor="Green" Font-Bold="true"></asp:Label></asp:TableCell>
            </asp:TableRow>
           </asp:Table>

    </div>
</asp:Content>
