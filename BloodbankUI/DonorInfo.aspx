﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BloodBank.Master" AutoEventWireup="true" CodeBehind="DonorInfo.aspx.cs" Inherits="BloodbankUI.DonorInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .auto-style3 {
        color: #003399;
        text-align: center;
            text-decoration: underline;
        }
    .auto-style2 {
            text-align: center;
            color: #000099;
        }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <p class="auto-style2">
                <asp:Button ID="viewDetails" runat="server" Text="View Details" BorderStyle="Solid" OnClick="viewDetails_Click"/>
                <asp:Button ID="regDonor" runat="server" Text="Register Donor" BorderStyle="Solid" OnClick="regDonor_Click"/>
                <asp:Button ID="bloodBag" runat="server" Text="Add/Update Blood Bag" BorderStyle="Solid" OnClick="bloodBag_Click"/>
                <asp:Button ID="weather" runat="server" Text="Nearby Blood Banks" BorderStyle="Solid" OnClick="weather_Click"/>
                <asp:Button ID="getStock" runat="server" Text="Get Stock" BorderStyle="Solid" OnClick="getStock_Click"/>
                <asp:Button ID="logout" runat="server" Text="Logout" BorderStyle="Solid" OnClick="logout_Click"/>
            </p>
    <h2 class="auto-style3"><strong>Donor Information</strong></h2>
    <div>
        <asp:DropDownList ID="ddlID" runat="server" OnSelectedIndexChanged="ddlID_SelectedIndexChanged1" AutoPostBack="true">
            <asp:ListItem>Select</asp:ListItem>
            <asp:ListItem>By RFID</asp:ListItem>
            <asp:ListItem>By DonorID</asp:ListItem>

        </asp:DropDownList>
        <br />
        <asp:Label ID="lblSelect" runat="server" Text="Please select an option between RFID No. and DonorID" Font-Bold="true" ForeColor="Red" Visible="false"></asp:Label>
        <asp:Label ID="lblRFID" runat="server" Text="RFID No.: " Font-Bold="true" Visible="false"></asp:Label>
        <asp:Label ID="lblDonorID" runat="server" Text="Donor ID: " Font-Bold="true" Visible="false"></asp:Label>
        <asp:TextBox ID="txtID" runat="server" Visible="false"></asp:TextBox>
        <asp:TextBox ID="txtDonorId" runat="server" Visible="false"></asp:TextBox>
        <br />
        <asp:Button ID="btnSubmitDonor" runat="server" Text="SUBMIT" Font-Bold="true" Visible="false" OnClick="btnSubmitDonor_Click"/>
        <asp:Button ID="btnSubmit" runat="server" Text="SUBMIT" Font-Bold="true" Visible="false" OnClick="btnSubmit_Click"/>
        <br />
        <asp:Label ID="lblMessage" runat="server" Text="" Visible="false" ForeColor="Red" Font-Bold ="true"></asp:Label>
        <asp:GridView ID="grvDonor" runat="server" Visible="false" AutoGenerateColumns="true" CellPadding="4" ForeColor="#333333">
            <AlternatingRowStyle BackColor="White" />
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <SortedAscendingCellStyle BackColor="#FDF5AC" />
            <SortedAscendingHeaderStyle BackColor="#4D0000" />
            <SortedDescendingCellStyle BackColor="#FCF6C0" />
            <SortedDescendingHeaderStyle BackColor="#820000" />
        </asp:GridView>
    </div>
</asp:Content>
