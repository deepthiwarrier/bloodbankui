﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="BloodbankUI.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
    <style type="text/css">
        .auto-style1 {
            color: #006600;
            text-align: center;
        }
        .auto-style2 {
            text-align: center;
        }
        .auto-style3 {
            color: #FFFFFF;
        }
        .auto-style4 {
            text-align: center;
            margin-left: 40px;
        }
        .auto-style5 {
            color: #000099;
        }
        .auto-style7 {
            text-align: center;
            color: #000099;
            text-decoration: underline;
        }
    </style>
</head>
<body style ="background-image:linear-gradient(rgba(255,255,255,0.5), rgba(255,255,255,0.5)), url('./Images/bloodbank4.png'); background-size: 100%;">
    <form id="form1" runat="server">
        <div class="auto-style1">
            <h1 class="auto-style5">
                <strong>RFID For Blood Banks</strong>&nbsp;&nbsp;</h1>
            <h3 class="auto-style7">
                Login For Lab Technicians</h3>
        </div>
        <div class="auto-style2">
            <h3 class="auto-style4"><span class="auto-style5">Username:&nbsp;&nbsp;</span><span class="auto-style3"> </span>
        <asp:TextBox ID="username" runat="server" Text="username" Width="210px"></asp:TextBox>
        <br />
                <span class="auto-style5">Password:&nbsp;&nbsp;&nbsp;</span><span class="auto-style3"> </span> <asp:TextBox ID="password" runat="server" Text="password" Width="210px"></asp:TextBox>
        <br />        
        <asp:Button ID="Button1" runat="server" Text="Login" OnClick="Button1_Click" />
        <br />
            </h3>            
        </div>
        <h3 class="auto-style7">
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Register.aspx">Register Donor</asp:HyperLink>
        </h3>
    </form>
</body>
</html>
